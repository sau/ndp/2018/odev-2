## SAKARYA ÜNİVERSİTESİ

## BİLGİSAYAR VE BİLİŞİM BİLİMLERİ FAKÜLTESİ

## BİLGİSAYAR MÜHENDİSLİĞİ BÖLÜMÜ

## Object oriented Programming Course

## Homework II


# Question

Bir futbol oyununda, oyun içinde çeşitli özellikler barından 4 defans 4 orta saha ve
2 forvet oyuncusu tasarlanmak istenmektedir.(Kaleci Yok)

Oyun için **AdSoyad, FormaNo, Hiz, Dayaniklilik, Pas, Sut, Yetenek,Kararlik,
DogalForm ve Sans** özelliklerini barındarın bir futbolcu sınıfı tanımlanmalıdır. Bu
futbolcu sınıfından defans,ortasaha ve forvet özelliklerinde 3 sınıf türetilmelidir.

**Defans oyuncusunda**

- PozisyonAlma
- Kafa
- Sicrama

**Orta Saha Oyuncusunda**

- UzunTop
- IlkDokunus
- Uretkenlik
- TopSurme
- OzelYetenek

**Forvet Oyuncusunda**

- Bitiricilik
- IlkDokunus
- Kafa
- ÖzelYetenek
- SogukKanlilik

özeliklerinin olması istenmektedir.


Yukarıda tanımlanan tüm özellikler, kendi sınıflarına ait kurucu fonksiyonlar
tanımlandığında, tanımlı olduğu sınıfa göre aşağıdaki değerleri rastgele olarak
alacaklardır.

**Futbolcu** sınıfı için oluştutulacak özellikler 50 -100 arasında

**Defans oyunuclarınd** a oluşturulacak özellikler 50 -90 arasında

**Orta saha oyuncular** ı için 60 -100 arasında

**Forvet oyuncuları** için 70 -100 arasında rastgele atanmalıdır.

Oyuna rastgele seçilecek bir oyuncu ile başlanmalı ve rastgele seçilen oyuncuya ait
Pasver() metodu çağrılmalıdır. Bu metod başarılı ise rastgele başka bir oyunucu
seçilerek, ona pas verildiği varsayımı ile top ona geçmeli, oyun devam etmelidir.

Bu olay 3 adet rastgele seçilen oyuncuya pas verilmesi şeklinde devam etmelidir.

Tüm Paslar başarlı olduysa 3 pasın sonunda rastgele seçilen (pas verilen) oyuncu
GolVurusu yapmalıdır. GolVurusu da başarılı ise GOLLLL, Oyuncu ismi ve sırt
numarası ekrana yazılmalıdır. PasVer ve GolVurusu başarılı olmadıysa ekrana bu
durumlara ait mesajlar basılmalıdır.

**Not:** Pasver Metodu başaılı ise rastgele seçilen oyuncunun pas veren oyuncu ile
aynı olmamasına özen gösterilmelidir. (Oyuncu kendine pas veremez)


**Örnek Çalışma Durumu**

Oyuncu seç =>Rastegele olarak 2 numaralı oyuncu seçildi

Pas Ver =>2 Numaralı oyuncu için PasVer metodunu çağır Pasver başarılı Başka
bir oyuncu seç (Örnek: Rastgele olarak 5 nolu oyuncu seçildi)

Pas Ver => 5 numarlı oyuncu için PasVer metodunu çağır Pasver başarılı Başka
bir oyuncu seç (Örnek: Rastgele olarak 7 nolu oyuncu seçildi)

Pas Ver =>7 Numaralı oyuncu için PasVer metodunu çağır Pasver başarılı Başka
bir oyuncu seç (Örnek: Rastgele olarak 9 nolu oyuncu seçildi)

Yukarıdaki PasVer metodlarının herhangi biri başarısız ise süreci
sonlandır ve durumu ekrana yaz.

GolVurusu => 9 Nolu oyuncu için GolVurusu Metodunu çalıştır.

**PasVer metodunun tanımlı olduğu sınıflara göre başarılı olma durumu**

**Futbolcu** sınıfı için
```
PasSkor = Pas * 0.3 + Yetenek * 0.3 + Dayaniklilik * 0.1 + DogalForm * 0.1 +
Sans * 0.2;
```
**Defans** sınıfı için
```
PasSkor = Pas * 0.3 + Yetenek * 0.3 + Dayaniklilik * 0.1 + DogalForm * 0.1 +
PozisyonAlma * 0.1 + Sans * 0.2;
```
**OrtaSaha** sınıfı için
```
PasSkor = Pas * 0.3 + Yetenek * 0.2 + OzelYetenek * 0.2 + Dayaniklilik * 0.1 +
DogalForm * 0.1 + UzunTop * 0.1 + TopSurme * 0.1 + Sans * 0.1;
```
**Forvet** sınıfı için
```
PasSkor = Pas * 0.3 + Yetenek * 0.2 + OzelYetenek * 0.2 + Dayaniklilik * 0.1 +
DogalForm * 0.1 + Sans * 0.1;
```
şeklinde oyuncunun özelliklerine göre hesaplanmalı **60 üzeri skor** başarlı pas
kabul

edilmedlir.

**GolVurusu metodunun tanımlı olduğu sınıflara göre başarılı olma durumu**

**Futbolcu** sınıfı için
```
GolSkor = Yetenek * 0.3 + Sut * 0.2 + Kararlilik * 0.1 + DogalForm * 0.1 +
Hiz * 0.1 + Sans * 0.2;
```
**_Defans_** _sınıfı için_
```
GolSkor = Yetenek * 0.3 + Sut * 0.2 + Kararlilik * 0.1 + DogalForm * 0.1 +
Kafa * 0.1 + Sicrama * 0.1 + Sans * 0.1;
```
**OrtaSaha** sınıfı için
```
GolSkor = Yetenek * 0.3 + OzelYetenek*0.2 + Sut * 0.2 + IlkDokunus * 0.1 +
Kararlilik * 0.1 + DogalForm * 0.1 + Sans * 0.1;
```
**Forvet** sınıfı için
```
GolSkor = Yetenek * 0.2 + OzelYetenek * 0.2 + Sut * 0.1 + Kafa * 0.1 + IlkDokunus *
0.1 + Bitiricilik * 0.1 + Sogukkanlilik * 0.1 + Kararlilik * 0.1 + DogalForm * 0.1 + Sans * 0.1;
```
Şeklinde hesaplanarak **70 puanın üzerinde skor** gol kabul edilmedilir.

**Örnek Kodlar**
```
public List<Futbolcu> takim = new List<Futbolcu>();

takim.Add(new Futbolcu("Mert Günok", 1));//kaleci

takim.Add(new Defans("Zeki Çelik",2));

takim.Add(new OrtaSaha("Emre Belezoğlu", 7));

takim.Add(new Forvet("Cenk Tosun", 11));

----

int FormaNo;

Boolean gololabilir = true;

for (int i = 1; i <= 3; i++)
{
....FormaNo = RastgeleSayi.Next(1, 11);

....if (!takim[FormaNo].Pasver())
....{
........gololabilir = false;
........break;
....}
}

if (gololabilir)
{
....FormaNo = RastgeleSayi.Next(1, 11);

....takim[FormaNo].GolVurusu();
}
```

# Ödev Teslim Kuralları

Soru için ayrı bir C# programı yazılacaktır. SABİS üzerinden teslim edilecektir.

**Zamanında teslim edilmeyen ödevden 0 puan alınacaktır.**

# KOPYA OÖDEVLER

Ödevler bireyseldir bu yüzden ödev için yazılan kodların kesinlikle paylaşılmaması gerekiyor. Bütün
ödevler karşılaştırılacaktır. Birbirine çok benzeyen ödevler 0 puan olarak değerlendirilecektir.

# SON TESLIİM TARIİHIİ


### Sistemde gösterilen tarih ve saattir.


# KOD DÜZENİ

Her C# dosyasının başında aşağıdaki yorum bloğu bulunacaktır. Yorum bulunmayan her dosya için 5
puan kırılacaktır. (pdf üzerinden kopyalayıp yapıştırmanız problem çıkartabilir)

/****************************************************************************
** SAKARYA ÜNİVERSİTESİ
** BİLGİSAYAR VE BİLİŞİM BİLİMLERİ FAKÜLTESİ
** BİLGİSAYAR MÜHENDİSLİĞİ BÖLÜMÜ
** NESNEYE DAYALI PROGRAMLAMA DERSİ
** 2014 - 2015 BAHAR DÖNEMİ
**
** ÖDEV NUMARASI..........:
** ÖĞRENCİ ADI............:
** ÖĞRENCİ NUMARASI.......:
** DERSİN ALINDIĞI GRUP...:
****************************************************************************/

Değişken isimleri anlamlı olmalıdır. Örneğin tek sayı adedini tutacak bir değişken için
```
int a;
```
yerine
```
int TekAdedi;
```
yazılmalıdır.

Her bir küme parantezinin altındaki kodlar ı yazmadan önce tab tuşuna basılarak boşluk bırakılmalıdır.

```
YANLIŞ
if(true)
{
int a =6;
printf("Merhaba");
if(false)
printf("Nasilsin");
}
```
```
DOĞRU
if(true)
{
....int a =6;
....printf("Merhaba");

....if(false)
........printf("Nasilsin");
}
```
Önemli döngü ve koşul işlemlerinden önce yapılan işlem hakkında yorum yazılmalıdır

```
YANLIŞ
int t = 0;
for(int i=0;i<100;i++)
t+=i;
```
```
DOĞRU
int Toplam = 0;
//1 den 100 e kadar olan sayılar toplanıyor
for(int i=0;i<100;i++)
Toplam+=i;
```

