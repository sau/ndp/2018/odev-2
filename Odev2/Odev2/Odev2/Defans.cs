using System;

namespace Odev2
{
    public class Defans : Futbolcu
    {
        private readonly ushort _kafa;
        private readonly ushort _pozisyonAlma;
        private readonly ushort _sicrama;

        public Defans(string adSoyad, ushort formaNo) : base(adSoyad, formaNo)
        {
            var random = new Random();
            const ushort min = 50;
            const ushort max = 90;

            _pozisyonAlma = (ushort) random.Next(min, max);
            _kafa = (ushort) random.Next(min, max);
            _sicrama = (ushort) random.Next(min, max);

            PasSkor = Pas * 0.3 +
                      Yetenek * 0.3 +
                      Dayaniklilik * 0.1 +
                      DogalForm * 0.1 +
                      _pozisyonAlma * 0.1 +
                      Sans * 0.2;

            GolSkor = Yetenek * 0.3 +
                      Sut * 0.2 +
                      Kararlilik * 0.1 +
                      DogalForm * 0.1 +
                      _kafa * 0.1 +
                      _sicrama * 0.1 +
                      Sans * 0.1;
        }
    }
}