using System;

namespace Odev2
{
    public class OrtaSaha : Futbolcu
    {
        private readonly ushort _ilkDokunus;
        private readonly ushort _ozelYetenek;
        private readonly ushort _topSurme;
        private ushort _uretkenlik;
        private readonly ushort _uzunTop;

        public OrtaSaha(string adSoyad, ushort formaNo) : base(adSoyad, formaNo)
        {
            var random = new Random();
            const ushort min = 60;
            const ushort max = 100;

            _uzunTop = (ushort) random.Next(min, max);
            _ilkDokunus = (ushort) random.Next(min, max);
            _uretkenlik = (ushort) random.Next(min, max);
            _topSurme = (ushort) random.Next(min, max);
            _ozelYetenek = (ushort) random.Next(min, max);

            PasSkor = Pas * 0.3 +
                      Yetenek * 0.2 +
                      _ozelYetenek * 0.2 +
                      Dayaniklilik * 0.1 +
                      DogalForm * 0.1 +
                      _uzunTop * 0.1 +
                      _topSurme * 0.1 +
                      Sans * 0.1;

            GolSkor = Yetenek * 0.3 +
                      _ozelYetenek * 0.2 +
                      Sut * 0.2 +
                      _ilkDokunus * 0.1 +
                      Kararlilik * 0.1 +
                      DogalForm * 0.1 +
                      Sans * 0.1;
        }
    }
}