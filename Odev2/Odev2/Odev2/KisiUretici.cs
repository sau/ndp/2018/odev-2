using System;
using System.Collections.Generic;
using System.Linq;

namespace Odev2
{
    public class KisiUretici
    {
        // Tekrarsız, rastgele kişi isimleri
        private static readonly string[] Kisiler =
        {
            "Abel Marsh",
            "Alejandro Owens",
            "Alton Glover",
            "Bennie Mccarthy",
            "Brett Carroll",
            "Bryan Chapman",
            "Cesar Chambers",
            "Clinton Richardson",
            "Dale Adams",
            "Don Jordan",
            "Donald Lee",
            "Dwight Thornton",
            "Ernest Banks",
            "Ernesto Sullivan",
            "Gerald Barnes",
            "Gregory Owen",
            "Jason Carr",
            "Jody Thompson",
            "Jonathan Cannon",
            "Jordan Vasquez",
            "Jorge Sanchez",
            "Joshua Nash",
            "Kristopher Stokes",
            "Larry Curtis",
            "Lee Rice",
            "Marshall Rodgers",
            "Maurice Mcguire",
            "Merle Romero",
            "Mitchell Gutierrez",
            "Oscar West",
            "Phil Delgado",
            "Philip Bennett",
            "Ramiro Lloyd",
            "Roberto Baldwin",
            "Salvatore Hughes",
            "Samuel Cummings",
            "Santiago Hopkins",
            "Shane Mcbride",
            "Shannon Sandoval",
            "Shawn Sanders",
            "Thomas Bryan",
            "Timothy Fowler",
            "Tom Summers",
            "Tony Hicks",
            "Travis Mendez",
            "Troy Andrews",
            "Virgil Vaughn",
            "Wallace Cruz",
            "Warren Fernandez",
            "Willis Graham"
        };

        private readonly Random _random;

        private List<string> _kisiListesi;
        private List<int> _numaraListesi;

        public KisiUretici()
        {
            _random = new Random();
            _kisiListesi = new List<string>();
            _numaraListesi = new List<int>();
        }

        /// <summary>
        ///     Kişi dizisinden kişi listesini oluşturur.
        /// </summary>
        private void KisiListesiniYenile()
        {
            _kisiListesi = new List<string>(Kisiler);
        }

        /// <summary>
        ///     Bir aralıktaki tamsayıları içeren numara listesini oluşturur.
        /// </summary>
        private void NumaraListesiniYenile()
        {
            _numaraListesi = Enumerable.Range(2, 99).ToList(); // Forma numaraları için geçerli aralık
        }

        /// <summary>
        ///     Bu metot her bir kişi adının tekrarsız olarak yalnızca bir defa döndürülmesini garanti eder. Fakat eğer
        ///     mevcut kişi veri tabanı aşılırsa liste sıfırlanır.
        /// </summary>
        /// <returns>Rastgele bir kişi ismi</returns>
        public string KisiAl()
        {
            var adet = _kisiListesi.Count;
            if (adet < 1) KisiListesiniYenile();

            var index = _random.Next(_kisiListesi.Count - 1);
            var kisi = _kisiListesi[index];
            _kisiListesi.RemoveAt(index); // Bu isim listeden silindiğinden artık döndürülemez.
            return kisi;
        }

        /// <summary>
        ///     Bu metot 2 ile 99 aralığındaki her bir numaranın tekrarsız olarak yalnızca bir defa döndürülmesini
        ///     garanti eder. Fakat eğer aralıktaki tüm değerler döndürülmüşse liste sıfırlanır.
        /// </summary>
        /// <returns>2 ile 99 aralığında rastgele bir sayı</returns>
        public ushort NumaraAl()
        {
            var adet = _numaraListesi.Count;
            if (adet < 1) NumaraListesiniYenile();

            var index = _random.Next(_numaraListesi.Count - 1);
            var numara = _numaraListesi[index];
            _numaraListesi.RemoveAt(index); // Bu numara listeden silindiğinden artık döndürülemez.
            return (ushort) numara;
        }
    }
}