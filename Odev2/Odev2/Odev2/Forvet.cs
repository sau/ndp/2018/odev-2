using System;

namespace Odev2
{
    public class Forvet : Futbolcu
    {
        private readonly ushort _bitiricilik;
        private readonly ushort _ilkDokunus;
        private readonly ushort _kafa;
        private readonly ushort _ozelYetenek;
        private readonly ushort _sogukkanlilik;

        public Forvet(string adSoyad, ushort formaNo) : base(adSoyad, formaNo)
        {
            var random = new Random();
            const ushort min = 70;
            const ushort max = 100;

            _bitiricilik = (ushort) random.Next(min, max);
            _ilkDokunus = (ushort) random.Next(min, max);
            _kafa = (ushort) random.Next(min, max);
            _ozelYetenek = (ushort) random.Next(min, max);
            _sogukkanlilik = (ushort) random.Next(min, max);

            PasSkor = Pas * 0.3 +
                      Yetenek * 0.2 +
                      _ozelYetenek * 0.2 +
                      Dayaniklilik * 0.1 +
                      DogalForm * 0.1 +
                      Sans * 0.1;

            GolSkor = Yetenek * 0.2 +
                      _ozelYetenek * 0.2 +
                      Sut * 0.1 +
                      _kafa * 0.1 +
                      _ilkDokunus * 0.1 +
                      _bitiricilik * 0.1 +
                      _sogukkanlilik * 0.1 +
                      Kararlilik * 0.1 +
                      DogalForm * 0.1 +
                      Sans * 0.1;
        }
    }
}