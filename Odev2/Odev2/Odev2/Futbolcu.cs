using System;

namespace Odev2
{
    public class Futbolcu
    {
        public Futbolcu(string adSoyad, ushort formaNo)
        {
            AdSoyad = adSoyad;
            FormaNo = formaNo;

            var random = new Random();
            const ushort min = 50;
            const ushort max = 100;

            Hiz = (ushort) random.Next(min, max);
            Dayaniklilik = (ushort) random.Next(min, max);
            Pas = (ushort) random.Next(min, max);
            Sut = (ushort) random.Next(min, max);
            Yetenek = (ushort) random.Next(min, max);
            Kararlilik = (ushort) random.Next(min, max);
            DogalForm = (ushort) random.Next(min, max);
            Sans = (ushort) random.Next(min, max);

            PasSkor = Pas * 0.3 +
                      Yetenek * 0.3 +
                      Dayaniklilik * 0.1 +
                      DogalForm * 0.1 +
                      Sans * 0.2;

            GolSkor = Yetenek * 0.3 +
                      Sut * 0.2 +
                      Kararlilik * 0.1 +
                      DogalForm * 0.1 +
                      Hiz * 0.1 +
                      Sans * 0.2;
        }

        public string AdSoyad { get; }
        public ushort FormaNo { get; }

        protected ushort Hiz { get; }

        protected ushort Dayaniklilik { get; }

        protected ushort Pas { get; }

        protected ushort Sut { get; }

        protected ushort Yetenek { get; }

        protected ushort Kararlilik { get; }

        protected ushort DogalForm { get; }

        protected ushort Sans { get; }

        protected double PasSkor { get; set; }

        protected double GolSkor { get; set; }

        public bool PasVer()
        {
            return PasSkor > 60;
        }

        public bool GolVurusu()
        {
            return GolSkor > 70;
        }

        public override string ToString()
        {
            return AdSoyad;
        }
    }
}