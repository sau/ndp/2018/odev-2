using System;
using System.Collections.Generic;

namespace Odev2
{
    internal class Program
    {
        private const ushort FutbolcuAdedi = 10;
        private readonly KisiUretici _kisiUretici;
        private readonly Random _random;
        private readonly List<Futbolcu> _takim;

        public Program()
        {
            _random = new Random();
            _kisiUretici = new KisiUretici();
            _takim = TakimAl(); // Rastgele futbolculardan meydana gelen takım listesi oluştur.
        }

        public static void Main()
        {
            new Program().Oynat();
        }

        /// <summary>
        ///     Maç simülasyonunu gerçekleştirir
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        private void Oynat()
        {
            var pasSonucu = Paslas(3); // Tüm oyuncular aralarında paslaştırılır.

            if (!pasSonucu) return; // Paslaşma sonucu başarısız ise program başarısız olmuştur.

            var futbolcu = _takim[_random.Next(FutbolcuAdedi)];

            Console.WriteLine($"{futbolcu.FormaNo} forma numaralı {futbolcu} topu aldı ve gole gidiyor!");

            if (futbolcu.GolVurusu())
            {
                Console.WriteLine($"GOOOOLLLL! {futbolcu.FormaNo} sırt numaralı {futbolcu}!");
            }
            else
            {
                var sebep = ""; // Eğer GolVurusu başarısız olmuşsa farklı mesajlar arasından rastgele biri yazdırılır.

                switch (_random.Next(4))
                {
                    case 0:
                        sebep = "Top az farkla dışarı gidiyor!";
                        break;
                    case 1:
                        sebep = "Kötü bir şut...";
                        break;
                    case 2:
                        sebep = "Kaleci gole izin vermiyor.";
                        break;
                    case 3:
                        sebep = "İstediği gibi bir vuruş yapamadı.";
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                Console.WriteLine($"{futbolcu}! {sebep}");
            }
        }

        /// <summary>
        ///     Rastgele oyuncular ile bir takım listesi oluşturur.
        /// </summary>
        /// <returns>Oyuncu listesi</returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        private List<Futbolcu> TakimAl()
        {
            var takim = new List<Futbolcu>();

            for (var i = 0; i < FutbolcuAdedi; i++)
            {
                Futbolcu futbolcu;
                var isim = _kisiUretici.KisiAl(); // Rastgele, tekrarsız bir isim alınır.
                var numara = _kisiUretici.NumaraAl(); // Rastgele, tekrarsız, 2-99 aralığında bir numara alınır.

                switch (_random.Next(4))
                {
                    case 0:
                        futbolcu = new Futbolcu(isim, numara);
                        break;
                    case 1:
                        futbolcu = new Defans(isim, numara);
                        break;
                    case 2:
                        futbolcu = new Forvet(isim, numara);
                        break;
                    case 3:
                        futbolcu = new OrtaSaha(isim, numara);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                takim.Add(futbolcu);
            }

            return takim;
        }

        /// <summary>
        ///     Futbolcular ardı sıra paslaştırılırlar ve buna göre sonuç hesaplanır.
        /// </summary>
        /// <param name="pasSayisi">Toplamda kaç adet paslaşma yapılacağı</param>
        /// <returns>Eğer sona kadar tüm paslaşmalar başarılı olursa true, değilse false</returns>
        private bool Paslas(ushort pasSayisi)
        {
            for (var i = 0; i < pasSayisi; i++)
            {
                // Futbolcunun kendine pas vermesini engellemek için en baştaki eleman alındıktan sonra listeden silinir
                // ve 0 hariç herhangi bir konuma rastgele yeniden eklenir. Sürekli en baştaki eleman alındığından aynı
                // elemanın üst üste iki defa gelmesi engellenmiş olur. Bu sayede bir futbolcunun daima kendinden farklı
                // birine pas vermesi sağlanır.

                var futbolcu = _takim[0];
                _takim.RemoveAt(0);
                var index = _random.Next(1, FutbolcuAdedi);
                _takim.Insert(index, futbolcu);

                Console.WriteLine($"{futbolcu.FormaNo} forma numaralı {futbolcu} topu aldı.");

                if (futbolcu.PasVer())
                {
                    Console.WriteLine($"{futbolcu} pas verdi.\n");
                }
                else
                {
                    Console.WriteLine($"{futbolcu} hatalı pas verdi, hücum başarısız.");
                    return false;
                }
            }

            return true;
        }
    }
}